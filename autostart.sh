#!/bin/bash

setxkbmap -layout dvorak -option ctrl:swapcaps &
nitrogen --restore &
dunst &
nm-applet &
xfce4-clipman &
gnome-screensaver &
volctl &
cbatticon &
# nextcloud & # seems to be buggy better to manually start it
redshift-gtk &
